function addRow(i) {
    var row=document.createElement('tr');
    row.id="row"+i;
    var td=document.createElement('td');
    td.id="img"+i;
    row.appendChild(td);
    td=document.createElement('td');
    td.id="name"+i;
    row.appendChild(td);
    td=document.createElement('td');
    td.id="color"+i;
    row.appendChild(td);
    td=document.createElement('td');
    td.id="size"+i;
    row.appendChild(td);
    td=document.createElement('td');
    td.id="qty"+i;
    row.appendChild(td);
    td=document.createElement('td');
    td.id="price"+i;
    row.appendChild(td);
    td=document.createElement('td');
    td.id="delete"+i;
    row.appendChild(td);
    
    return row;    
}


function initTable() {
    var items=localStorage.getItem('basket')?Array.from(JSON.parse(localStorage.getItem('basket'))):false;
        var table=document.querySelector('table tbody');
        
   if(items.length>0){
      
       for(var i=0;i<items.length;i++){
             var row=addRow(i);
           
            for(var value in items[i]){
             
            var column=row.querySelector('#'+value+i);
            if(value=='article'){
                continue;
            }
           
                column.appendChild(switchProp(value,items[i]));
            
       
            }
            var column=row.querySelector('#delete'+i);;
            column.appendChild(switchProp("delete",null));
             var column=row.querySelector('#qty'+i);;
            column.appendChild(switchProp("qty",null));
           
            table.appendChild(row);
       }
       
   }else{
     listCounter(table);
        
   }
initPopup();
   sumCounter();
   //nav
   (function () {
       var a=document.querySelector('.navigation a').onclick=function () {
           location.replace('../home');
       }
       
       var btn=document.querySelector('.navigation button').onclick=function () {
           Basket.clearItems();
           location.replace('#ty');
       }
   })()
}
function listCounter(table) {
      
       var item=localStorage.getItem('basket')?Array.from(JSON.parse(localStorage.getItem('basket'))).length:0;
 
    if(item==0){
          var row=document.createElement('tr');
       var column=document.createElement('td');
          var h2=document.createElement('h2');
          column.style.width="100%";
          h2.innerHTML="Nothing here...yet..";
          h2.style.textAlign="center";
          h2.style.marginBottom="2%";
          column.appendChild(h2);
          row.appendChild(column);
          table.appendChild(row);       
          
       document.querySelector('.sum').style.display="none";
    }
}

function switchProp(prop,obj) {
   
   var el=prop?undefined:document.createElement('span');
    switch(prop){
        case "img":{
            el=document.createElement('img');
            el.style="width:60px;height:60px;";
           el.src=obj[prop];
           break;
        }
        case "name":{
            el=document.createElement('div');
           var  h3=document.createElement('h3');
            h3.innerHTML=obj['name'];
            h3.id="ProductName";
            el.appendChild(h3);
            
             var sp=document.createElement('span');
             sp.classList.add('subheader-small');
           sp.innerHTML=obj["article"];
           el.appendChild(sp);
            break;
            
        }
        case "color":{
            el=document.createElement('span')
            el.innerHTML=obj['color'];
            
       break;
        }
        case "article":{
           
            break;
        }
        case "price":{
            el=document.createElement('span');
            el.innerHTML=obj["price"];
                        break;
        }
        case "size":{
            el=document.createElement('span');
            el.innerHTML=obj['size'];
            break;
        }
        case "delete":{
            el=document.createElement('div');
            el.innerHTML="&times;"
            el.style.paddingLeft="10%";
            el.classList.add('delete');
            el.onclick=function (e) {
                deleteItem(e);
            }
            break;
        }
        case "qty":{
            el=document.createElement('input');
            el.type="number";
            el.value=1;
            el.min=1;
            el.onchange=function (e) {
                sumCounter();
            }
           el.style.width="20%"
            break;
        }
      }
    return el;
}
function initPopup(id) {
   
    
}
function deleteItem(e) {
    
  document.querySelector('.popup').classList.toggle('display');
   var id=e.target.parentNode.id.split(/[delete]/);
   id=parseInt(id[id.length-1]);
  var table=document.querySelector('table tbody');
  var a= document.querySelectorAll('.popup a');
     a[0].onclick=function (even) {
            event.preventDefault();
    Basket.removeItem(id);         
  document.querySelector('.popup').classList.toggle('display');
   
   table.removeChild(e.target.parentNode.parentNode);
   
     listCounter(table);
     
    sumCounter();
        }
        a[1].onclick=function () {
            event.preventDefault();
            
  document.querySelector('.popup').classList.toggle('display');
        }
  
   
    
}
function sumCounter() {
    var qty=document.querySelectorAll('input[type="number"]');
    var sumDiv=document.querySelector('#sum');
   var sum=0;
   var value=document.querySelectorAll('td[id^="price"] span');
   console.log(qty);
    for (var i=0;i<qty.length;i++){
            
          console.log(value);
            sum+=parseInt(qty[i].value)*parseFloat(value[i].innerHTML);
      
    }
   sumDiv.innerHTML=sum +"&#8364;";
}